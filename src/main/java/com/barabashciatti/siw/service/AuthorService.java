package com.barabashciatti.siw.service;

import org.springframework.transaction.annotation.Transactional;

import com.barabashciatti.siw.model.Author;
import com.barabashciatti.siw.repository.AuthorRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Transactional
@Service
public class AuthorService {

	@Autowired
	private AuthorRepository authorRepository;
	
	@Transactional
	public Author save(Author author) {
		return this.authorRepository.save(author);
	}
    
	@Transactional
	public Author authorPerId(Long id) {
		return this.authorRepository.findById(id).get();
	}
	
	@Transactional
	public List<Author> tutti(){
		return (List<Author>) this.authorRepository.findAll();
	}

	@Transactional
	public void removePerId(Long id) {
		this.authorRepository.deleteById(id);
	}
	
	@Transactional
	public List<Author> Last5Authors() {
		return this.authorRepository.findTop3ByOrderByIdDesc();
	}

		
	
	
}
