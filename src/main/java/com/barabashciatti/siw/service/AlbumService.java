package com.barabashciatti.siw.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.barabashciatti.siw.model.Album;
import com.barabashciatti.siw.model.Author;
import com.barabashciatti.siw.model.Photo;
import com.barabashciatti.siw.repository.AlbumRepository;


@Transactional
@Service
public class AlbumService {
	@Autowired
	private AlbumRepository albumRepository;
	
	@Transactional
	public Album save(Album album) {
		return this.albumRepository.save(album);
	}
	
	@Transactional
	public Album albumPerId(Long id) {
		return this.albumRepository.findById(id).get();
	}
	
	@Transactional
	public List<Album> albumPerAuthorId(Long id){
		return (List<Album>) this.albumRepository.findByAuthorId(id);
	}
	
	@Transactional
	public void deleteAlbumperId(Long id) {
		this.albumRepository.deleteById(id);
	}
	
	@Transactional 
	public List<Album> OrderAllById(){
		return (List<Album>) this.albumRepository.findTop5ByOrderByIdDesc();
	}

	public void removePerId(Long id) {
		 this.albumRepository.deleteById(id);
		
	}

	public List<Album> tutti() {
		return (List<Album>) this.albumRepository.findAll();
	}
	
}
