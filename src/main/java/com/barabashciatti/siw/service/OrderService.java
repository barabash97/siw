package com.barabashciatti.siw.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.barabashciatti.siw.model.Order;
import com.barabashciatti.siw.repository.OrderRepository;

@Transactional
@Service
public class OrderService {
	@Autowired
	private OrderRepository orderRepository;

	public Order save(Order order) {
		return this.orderRepository.save(order);
	}
	
	public Order findById(Long id) {
		return this.orderRepository.findById(id).get();
	}
	
	public List<Order> tutti(){
		return (List<Order>) this.orderRepository.findAll();
	}
}
