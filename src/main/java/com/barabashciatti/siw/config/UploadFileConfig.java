package com.barabashciatti.siw.config;

import java.io.File;


public class UploadFileConfig {
	public static final String rootUploadingDir = "uploads";
	public static final String uploadingDir = "" + System.getProperty("user.dir") + File.separator
			+ "src/main/resources/static/" + File.separator + UploadFileConfig.rootUploadingDir + File.separator;
}
