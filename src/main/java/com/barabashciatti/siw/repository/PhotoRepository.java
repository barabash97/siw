package com.barabashciatti.siw.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.barabashciatti.siw.model.Photo;


public interface PhotoRepository extends CrudRepository<Photo, Long> {
	
	public List<Photo> findByAlbumId(Long album_id);
	//public Photo findById(Long id);
	
	public List<Photo> findTop20ByOrderByIdDesc();
}

