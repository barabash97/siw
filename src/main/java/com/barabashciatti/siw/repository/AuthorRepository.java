package com.barabashciatti.siw.repository;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.barabashciatti.siw.model.Author;
import com.barabashciatti.siw.model.Photo;

public interface AuthorRepository extends CrudRepository<Author, Long> {
	
	public List<Author> findTop3ByOrderByIdDesc();
	
	
	
}
