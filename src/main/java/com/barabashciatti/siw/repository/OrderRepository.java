package com.barabashciatti.siw.repository;

import org.springframework.data.repository.CrudRepository;

import com.barabashciatti.siw.model.Order;


public interface OrderRepository extends CrudRepository<Order, Long> {

}
