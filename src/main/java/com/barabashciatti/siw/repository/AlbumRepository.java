package com.barabashciatti.siw.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.barabashciatti.siw.model.Album;
import com.barabashciatti.siw.model.Photo;

public interface AlbumRepository extends CrudRepository<Album, Long> {
	
	public List<Album> findByAuthorId(Long author_id);
	
	public List<Album> findTop5ByOrderByIdDesc();
}
