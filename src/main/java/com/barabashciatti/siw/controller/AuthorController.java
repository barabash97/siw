package com.barabashciatti.siw.controller;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.barabashciatti.siw.model.Album;
import com.barabashciatti.siw.model.Author;
import com.barabashciatti.siw.model.Photo;
import com.barabashciatti.siw.service.AlbumService;
import com.barabashciatti.siw.service.AuthorService;
import com.google.common.io.Files;

@Controller
public class AuthorController {

	@Autowired
	private AuthorService authorService;

	public static final String rootUploadingDir = "uploads";
	public static final String uploadingDir = "" + System.getProperty("user.dir") + File.separator
			+ "src/main/resources/static" + File.separator + AuthorController.rootUploadingDir + File.separator;

	@RequestMapping(value = "/author", method = RequestMethod.GET)
	public ModelAndView index() {
		ModelAndView m = new ModelAndView();
		List<Author> authors = this.authorService.tutti();
		m.addObject("authors", authors);
		m.setViewName("author/list");
		return m;
	}

	@RequestMapping(value = "/author/form", method = RequestMethod.GET)
	public ModelAndView form() {
		ModelAndView m = new ModelAndView();
		Author author = new Author();
		m.addObject("author", author);
		m.setViewName("author/form");
		return m;
	}

	@RequestMapping(value = "/author/{id}", method = RequestMethod.GET)
	public ModelAndView getAuthor(@PathVariable("id") Long id) {
		ModelAndView m = new ModelAndView();
		if (id != null) {
			Author a = this.authorService.authorPerId(id);
			m.addObject("author", a);
			m.setViewName("author/get");
		} else {
			m.addObject("author", new Author());
		}
		return m;
	}

	@RequestMapping(value = "/author", method = RequestMethod.POST)
	public ModelAndView create(@Valid Author author, BindingResult bindingResult) {
		ModelAndView m = new ModelAndView();
		if (bindingResult.hasErrors()) {
			m.setViewName("author/form");
		} else {
			authorService.save(author);
			m.addObject("successMessage", " Il fotografo è stato registrato con successo");
			m.setViewName("author/get");
		}

		return m;
	}

	@RequestMapping(value = "/author/{id}", method = RequestMethod.DELETE)
	public ModelAndView deleteAuthor(@PathVariable("id") Long id) {
		ModelAndView m = new ModelAndView();
		if (id != null) {
			this.authorService.removePerId(id);
			m.addObject("authors", this.authorService.tutti());
			m.setViewName("author/list");
		} else {
			m.setViewName("errors/404");
		}
		return m;
	}
	
	@RequestMapping(value = "/author/{id}/avatar", method = RequestMethod.POST)
	public String uploadingPost(@PathVariable("id") Long id,
			@RequestParam("uploadingFiles") MultipartFile[] uploadingFiles) throws IOException {

		Author author = this.authorService.authorPerId(id);
		for (MultipartFile uploadedFile : uploadingFiles) {
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			String extension = Files.getFileExtension(uploadedFile.getOriginalFilename());
			String filename = "avatar_" + author.getId() + "_" + timestamp.getTime() + "." + extension;
//            File file = new File(uploadingDir + uploadedFile.getOriginalFilename());
			File file = new File(uploadingDir + filename);

			author.setAvatar(File.separator + PhotoController.rootUploadingDir + "/" + filename);
			this.authorService.save(author);
			uploadedFile.transferTo(file);
			break;
		}

		return "redirect:/author/" + author.getId() + "";

	}

	@RequestMapping(value = "/author/{id}/avatar", method = RequestMethod.GET)
	public ModelAndView getUploading(@PathVariable("id") Long id) {
		Author author = this.authorService.authorPerId(id);
		ModelAndView m = new ModelAndView();
		File file = new File(uploadingDir);
		m.addObject("author", author);
		m.addObject("files", file.listFiles());
		m.setViewName("author/avatar");
		return m;
	}
}
