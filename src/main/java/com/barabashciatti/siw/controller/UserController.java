package com.barabashciatti.siw.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserController {

	@RequestMapping(value="/info/{id}", method=RequestMethod.GET)
	public String info(@PathVariable("id") Long id,Model model) {
		
		model.addAttribute("id", id);
		
		return "info";
	}
	
	@RequestMapping(value="/user/{id}", method=RequestMethod.GET)
	public String user(@PathVariable("id") Long id,Model model) {
		
		model.addAttribute("id", id);
		
		return "userInfo";
	}
	
	@RequestMapping(value = {"/logout"}, method = RequestMethod.GET)
	public String logoutDo(HttpServletRequest request,HttpServletResponse response){
	HttpSession session= request.getSession(false);
	    SecurityContextHolder.clearContext();
	         session= request.getSession(false);
	        if(session != null) {
	            session.invalidate();
	        }
	        for(Cookie cookie : request.getCookies()) {
	            cookie.setMaxAge(0);
	        }

	    return "/";
	}
}
