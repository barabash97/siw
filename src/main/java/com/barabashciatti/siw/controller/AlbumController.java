package com.barabashciatti.siw.controller;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.barabashciatti.siw.config.UploadFileConfig;
import com.barabashciatti.siw.model.Album;
import com.barabashciatti.siw.model.Author;
import com.barabashciatti.siw.model.Photo;
import com.barabashciatti.siw.service.AlbumService;
import com.barabashciatti.siw.service.AuthorService;
import com.barabashciatti.siw.service.PhotoService;
import com.google.common.io.Files;

@Controller
public class AlbumController {

	@Autowired
	private AuthorService authorService;

	@Autowired
	private AlbumService albumService;

	@Autowired
	private PhotoService photoService;

	@RequestMapping(value = "/author/{idAuthor}/album/form", method = RequestMethod.GET)
	public ModelAndView create(@PathVariable("idAuthor") Long idAuthor) {
		ModelAndView m = new ModelAndView();
		Author author = this.authorService.authorPerId(idAuthor);
		if (author == null) {
			author = new Author();
		}
		Album album = new Album();
		m.addObject("album", album);
		m.addObject("author", author);
		m.setViewName("album/form");
		return m;
	}

	@RequestMapping(value = "/author/{idAuthor}/album", method = RequestMethod.POST)
	public ModelAndView save(@PathVariable("idAuthor") Long idAuthor, @Valid @ModelAttribute("album") Album album,
			@RequestParam("uploadingImage") MultipartFile[] uploadingFiles, BindingResult bindingResult)
			throws IOException {

		ModelAndView m = new ModelAndView();
		Author author = this.authorService.authorPerId(idAuthor);

		if (bindingResult.hasErrors()) {
			m.setViewName("album/form");
		} else {

			album.setAuthor(author);
			this.albumService.save(album);
			album = this.saveImage(album, uploadingFiles);
			m.setViewName("album/single");
		}

		m.addObject("author", author);
		m.addObject("album", album);

		return m;
	}

	@RequestMapping(value = "/author/{idAuthor}/album", method = RequestMethod.GET)
	public ModelAndView index(@PathVariable("idAuthor") Long idAuthor) {

		ModelAndView m = new ModelAndView();
		Author author = this.authorService.authorPerId(idAuthor);
		if (author == null) {
			m.setViewName("errors/404");
			return m;
		}

		List<Album> albums = this.albumService.albumPerAuthorId(author.getId());

		m.addObject("albums", albums);
		m.addObject("author", author);
		m.setViewName("album/list");
		return m;
	}

	@RequestMapping(value = "/album/{id}", method = RequestMethod.GET)
	public ModelAndView get(@PathVariable("id") Long id) {
		ModelAndView m = new ModelAndView();

		Album album = this.albumService.albumPerId(id);

		if (album == null) {
			album = new Album();
		}

		List<Photo> photos = album.getPhoto();
		if (photos == null) {
			photos = new LinkedList<>();
		}
		m.addObject("album", album);
		m.addObject("photos", photos);
		m.setViewName("album/single");
		return m;
	}

	public Album saveImage(Album album, MultipartFile[] uploadingFiles) throws IOException {

		for (MultipartFile uploadedFile : uploadingFiles) {
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			String extension = Files.getFileExtension(uploadedFile.getOriginalFilename());
			String filename = "albumImage_" + album.getId() + "_" + timestamp.getTime() + "." + extension;
//            File file = new File(uploadingDir + uploadedFile.getOriginalFilename());
			File file = new File(UploadFileConfig.uploadingDir + filename);

			album.setAlbumImage(File.separator + UploadFileConfig.rootUploadingDir + "/" + filename);
			this.albumService.save(album);
			uploadedFile.transferTo(file);
			break;
		}

		return album;
	}

	@RequestMapping(value = "/album/{id}", method = RequestMethod.DELETE)
	public String deleteAlbum(@PathVariable("id") Long id) {
		Album album = this.albumService.albumPerId(id);
		Author author = album.getAuthor();
		if (album != null) {
			this.albumService.removePerId(album.getId());
			return "redirect:/author/" + author.getId() + "/album";
		}
		return "redirect:/index";

	}
}
