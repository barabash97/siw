package com.barabashciatti.siw.controller;

import java.io.File;
import java.io.IOException;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.barabashciatti.siw.model.Album;
import com.barabashciatti.siw.model.Author;
import com.barabashciatti.siw.model.Photo;
import com.barabashciatti.siw.service.AlbumService;
import com.barabashciatti.siw.service.AuthorService;
import com.barabashciatti.siw.service.PhotoService;
import com.google.common.io.Files;

@Controller
public class PhotoController {

	@Autowired
	private PhotoService photoService;
	
	@Autowired
    private AuthorService authorService;

	@Autowired
	private AlbumService albumService;
	public static final String rootUploadingDir = "uploads";
	public static final String uploadingDir = "" + System.getProperty("user.dir") + File.separator
			+ "src/main/resources/static" + File.separator + PhotoController.rootUploadingDir + File.separator;

	@RequestMapping(value = "/album/{id}/upload", method = RequestMethod.GET)
	public ModelAndView getUploading(@PathVariable("id") Long id) {
		Album album = this.albumService.albumPerId(id);
		ModelAndView m = new ModelAndView();
		File file = new File(uploadingDir);
		m.addObject("album", album);
		m.addObject("files", file.listFiles());
		m.setViewName("album/upload");
		return m;
	}

	@RequestMapping(value = "/album/{id}/upload", method = RequestMethod.POST)
	public String uploadingPost(@PathVariable("id") Long id,
			@RequestParam("uploadingFiles") MultipartFile[] uploadingFiles) throws IOException {

		Album album = this.albumService.albumPerId(id);
		for (MultipartFile uploadedFile : uploadingFiles) {
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			String extension = Files.getFileExtension(uploadedFile.getOriginalFilename());
			String filename = "album_" + album.getId() + "_" + timestamp.getTime() + "." + extension;
//            File file = new File(uploadingDir + uploadedFile.getOriginalFilename());
			File file = new File(uploadingDir + filename);

			Photo p = new Photo();
			p.setPath(File.separator + PhotoController.rootUploadingDir + "/" + filename);
			p.setAlbum(album);
			this.photoService.save(p);
			uploadedFile.transferTo(file);
		}

		return "redirect:/album/" + album.getId() + "";
	}

	@RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
	public String show20Photos(Model model) {
		List<Photo> photo = this.photoService.OrderAllById();
		List<Author> authors = this.authorService.Last5Authors();

		model.addAttribute("authors", authors);
		model.addAttribute("photos", photo);

		return "home";
	}

}
