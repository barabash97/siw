package com.barabashciatti.siw.controller;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.barabashciatti.siw.model.Order;
import com.barabashciatti.siw.model.Photo;
import com.barabashciatti.siw.service.OrderService;
import com.barabashciatti.siw.service.PhotoService;

@Controller
public class OrderController {

	@Autowired
	private PhotoService photoService;
	@Autowired
	private OrderService orderService;
	@Autowired
	private HttpSession httpSession;

	private String keySessionPhoto = "orderPhoto";

	@RequestMapping(value = "/order", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView m = new ModelAndView();
		Order order = new Order();
		Set<Photo> photos = this.getPhotosFromSession();
		m.addObject("order", order);
		m.addObject("photos", photos);
		m.setViewName("order/form");
		return m;
	}
	
	@RequestMapping(value = "/orders", method = RequestMethod.GET)
	public ModelAndView index() {
		ModelAndView m = new ModelAndView();
		List<Order> orders = this.orderService.tutti();
		m.addObject("orders", orders);
		m.setViewName("order/list");
		return m;
	}
	
	@RequestMapping(value = "/order/{id}", method = RequestMethod.GET)
	public ModelAndView show(@PathVariable("id") Long id) {
		ModelAndView m = new ModelAndView();
		Order order = this.orderService.findById(id);
		
		if(order == null) {
			order = new Order();
		}
		m.addObject("order", order);
		m.setViewName("order/single");
		return m;
	}
	
	@RequestMapping(value = "/order", method = RequestMethod.POST)
	public ModelAndView save(@Valid Order order, BindingResult bindingResult) {
		ModelAndView m = new ModelAndView();
		Set<Photo> photos = this.getPhotosFromSession();
		if (!bindingResult.hasErrors() || photos.size() == 0) {
			order = this.orderService.save(order);
			order.setPhotos(photos);
			this.orderService.save(order);
			this.resetPhotoSession();
			m.setViewName("home");
		} else {
			m.addObject("order", order);
			m.addObject("photos", photos);
			m.setViewName("order/form");
		}

		return m;
	}

	@RequestMapping(value = "/order/photo/{id}", method = RequestMethod.GET)
	public ResponseEntity<Boolean> addPhotoToOrder(@PathVariable("id") Long id) {
		Photo photo = this.photoService.getPhotoById(id);
		if (photo != null) {
			this.addPhotoToSession(photo);
		} else {
			return ResponseEntity.ok(false);
		}
		return ResponseEntity.ok(true);
	}
	
	@RequestMapping(value = "/order/deletePhoto/{id}", method = RequestMethod.GET)
	public ResponseEntity<Boolean> deletePhotoFromOrder(@PathVariable("id") Long id) {
		Photo photo = this.photoService.getPhotoById(id);
		if (photo != null) {
			this.removePhotoToSession(photo);
		} else {
			return ResponseEntity.ok(false);
		}
		return ResponseEntity.ok(true);
	}
	
	@RequestMapping(value = "/order/count", method = RequestMethod.GET)
	public ResponseEntity<Integer> countPhotoToOrder() {
		Set<Photo> photos = this.getPhotosFromSession();
		Integer count = photos.size();
		return ResponseEntity.ok(count);
	}

	@SuppressWarnings("unchecked")
	public Set<Photo> getPhotosFromSession() {
		Set<Photo> photos = (Set<Photo>) httpSession.getAttribute(this.keySessionPhoto);
		if (photos == null) {
			photos = new HashSet<>();
			httpSession.setAttribute(this.keySessionPhoto, photos);
		}
		return photos;

	}

	@SuppressWarnings("deprecation")
	public void addPhotoToSession(Photo photo) {
		Set<Photo> photos = getPhotosFromSession();

		Iterator<Photo> it = photos.iterator();
		while (it.hasNext()) {
			Photo item = it.next();
			if (item.getId() == photo.getId()) {
				return;
			}
		}

		photos.add(photo);

		this.httpSession.putValue(this.keySessionPhoto, photos);
	}

	@SuppressWarnings("deprecation")
	public void removePhotoToSession(Photo photo) {
		Set<Photo> photos = getPhotosFromSession();
		Set<Photo> newPhotos = new HashSet<>();
		
		Iterator<Photo> it = photos.iterator();
		while (it.hasNext()) {
			Photo item = it.next();
			if (item.getId() != photo.getId()) {
				newPhotos.add(item);
			} 
		}
		this.httpSession.removeAttribute(this.keySessionPhoto);
		this.httpSession.putValue(this.keySessionPhoto, newPhotos);
	}
	
	public void resetPhotoSession() {
		Set<Photo> photos = new HashSet<>();
		httpSession.setAttribute(this.keySessionPhoto, photos);
	}
}
