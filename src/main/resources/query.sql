INSERT INTO users (ID, enabled, password, username) 
VALUES (1, 1, "$2a$10$exYnuoZTcdJ9swsLf3ISneSCS1tkLpDpFy4KAgoMzrgn5fhVHGoM2", "admin");

INSERT INTO users (ID, enabled, password, username) 
VALUES (2, 1, "$2a$10$exYnuoZTcdJ9swsLf3ISneSCS1tkLpDpFy4KAgoMzrgn5fhVHGoM2", "user");

INSERT INTO authority (ID, authority) VALUES (1, "ROLE_ADMIN");
INSERT INTO authority (ID, authority) VALUES (2, "ROLE_USER");
INSERT INTO authorities_users (user_id, authority_id) VALUES (1,1);
INSERT INTO authorities_users (user_id, authority_id) values (1,2);
INSERT INTO authorities_users (user_id, authority_id) VALUES (2,2);
