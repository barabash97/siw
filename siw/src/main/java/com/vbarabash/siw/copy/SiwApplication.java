package com.vbarabash.siw.copy;

import java.io.File;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.vbarabash.siw.controller.PhotoController;

@SpringBootApplication
public class SiwApplication {

	public static void main(String[] args) {
		new File(PhotoController.uploadingDir).mkdirs();
		SpringApplication.run(SiwApplication.class, args);
	}

}
