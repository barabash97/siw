package com.vbarabash.siw.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.vbarabash.siw.model.Photo;


public interface PhotoRepository extends CrudRepository<Photo, Long> {
	
	public List<Photo> findByAlbumId(Long album_id);
	//public Photo findById(Long id);
	
	public List<Photo> findTop20ByOrderByIdDesc();
}

