package com.vbarabash.siw.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.vbarabash.siw.model.Album;
import com.vbarabash.siw.model.Photo;

public interface AlbumRepository extends CrudRepository<Album, Long> {
	
	public List<Album> findByAuthorId(Long author_id);
	
	public List<Album> findTop5ByOrderByIdDesc();
}
