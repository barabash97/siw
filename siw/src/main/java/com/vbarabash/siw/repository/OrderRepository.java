package com.vbarabash.siw.repository;

import org.springframework.data.repository.CrudRepository;

import com.vbarabash.siw.model.Order;


public interface OrderRepository extends CrudRepository<Order, Long> {

}
