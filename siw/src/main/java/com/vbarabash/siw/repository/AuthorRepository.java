package com.vbarabash.siw.repository;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.vbarabash.siw.model.Author;

public interface AuthorRepository extends CrudRepository<Author, Long> , AuthorRepositoryCustom {
	
	
	
	public List<Author> findTop3ByOrderByIdDesc();
	
	
	
	
}
