package com.vbarabash.siw.repository;

import java.util.List;

import com.vbarabash.siw.model.Author;

public interface AuthorRepositoryCustom {

	public List<Author> searchByNome(String nome);
}

