package com.vbarabash.siw.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.vbarabash.siw.model.Author;

@Repository
public class AuthorRepoSitoryCustomImpl implements AuthorRepositoryCustom {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<Author> searchByNome(String nome) {

		Query query = this.entityManager
				.createNativeQuery("SELECT au.* FROM  Author as au  WHERE" + " au.firstname LIKE ?", Author.class);
		query.setParameter(1, nome + "%");

		return query.getResultList();
	}

}
