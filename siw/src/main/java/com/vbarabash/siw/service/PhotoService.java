package com.vbarabash.siw.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vbarabash.siw.model.Photo;
import com.vbarabash.siw.repository.PhotoRepository;


@Transactional
@Service
public class PhotoService {
	@Autowired
	private PhotoRepository photoRepository;
	
	public Photo save(Photo order) {
		return this.photoRepository.save(order);
	}
	
	public List<Photo> tutti(Long album_id){
		return this.photoRepository.findByAlbumId(album_id);
	}
	
	@Transactional
	public Photo getPhotoById(Long id) {
		return this.photoRepository.findById(id).get();
	}
	
	@Transactional 
	public List<Photo> OrderAllById(){
		return (List<Photo>) this.photoRepository.findTop20ByOrderByIdDesc();
	}
}
