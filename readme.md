username: admin
password: 123456


RDS configuration:
spring.datasource.url=jdbc:mysql://siw2019av.cbnw7cwuc5rd.eu-west-2.rds.amazonaws.com/progetto?serverTimezone=UTC
spring.datasource.username=aleciatti
spring.datasource.password=Alessia12

SQL di base:
INSERT INTO users (ID, enabled, password, username) 
VALUES (1, 1, "$2a$10$exYnuoZTcdJ9swsLf3ISneSCS1tkLpDpFy4KAgoMzrgn5fhVHGoM2", "admin");

INSERT INTO users (ID, enabled, password, username) 
VALUES (2, 1, "$2a$10$exYnuoZTcdJ9swsLf3ISneSCS1tkLpDpFy4KAgoMzrgn5fhVHGoM2", "user");

INSERT INTO authority (ID, authority) VALUES (1, "ROLE_ADMIN");
INSERT INTO authority (ID, authority) VALUES (2, "ROLE_USER");
INSERT INTO authorities_users (user_id, authority_id) VALUES (1,1);
INSERT INTO authorities_users (user_id, authority_id) values (1,2);
INSERT INTO authorities_users (user_id, authority_id) VALUES (2,2);


Progettto SIW 2019:
 - Anton Beketov  (502553) - anton.beketov@gmail.com
 - Volodymyr Barabash (508763) - barabash97@gmail.com
 - Alessia Ciatti (492422) - alessia.ciatti19@gmail.com
 
 https://bitbucket.org/barabash97/siw/src/master/
 

- Inserite nuove funzionalità di ricerca per nome su autore e anche su album 

-  Funzioni in fase di sviluppo nel branch: AntonSearch

